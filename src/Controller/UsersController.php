<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('add');
    }

    public function isAuthorized()
    {
        // $user = $this->Auth->user();
        // if($user['_matchingData']['UsersPermissions']['permission']==);

        // // All registered users can add articles
        // if ($this->request->getParam('action') === 'add') {
        //     return true;
        // }

        // // The owner of an article can edit and delete it
        // if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
        //     $articleId = (int)$this->request->getParam('pass.0');
        //     if ($this->Articles->isOwnedBy($articleId, $user['id'])) {
        //         return true;
        //     }
        // }

        return parent::isAuthorized();
    }

    public function login()
    {
        if($this->Auth->user())
            return $this->redirect(['action' => 'view', $this->Auth->user('id')]);
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->Error(__('Invalid Credentials or Your account is not activated Yet'));
        };
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    protected function user_permission()
    {        
        $user = $this->Auth->user();
        return $user['_matchingData']['UsersPermissions']['permission'];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public $paginate = [
        'limit' => 5,
        'order' => [
          'Users.created' => 'desc'
       ]
    ];
    public function searchHelper()
    {
        if ($this->request->is(['ajax'])) {
            $varname = 'akudgi';
            $this->set(compact('varname'));
            // echo json_encode(compact('varname'));
            // $this->set('_serialize', ['varname']);
            // $this->autoRender = false;
            // die('in');
        }
            // die('out');
    }

    public function sortedList()
    {
        if ($this->request->is(['ajax'])) {
            // die($this->request->getParam('user_data'));

            $query = $this->Users->find();
            if( $s = $this->request->query('user_data') )
                $query->where(['name LIKE' => '%' . $s . '%']);
           
            $users = $this->paginate($query, ['limit' => 10]);

            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        }
    }
    public function index()
    {
        if($this->user_permission() == 3)
            return $this->redirect(['action' => 'view', $this->Auth->user('id')]);

        $query = $this->Users->find();
        if( $s = $this->request->query('s') )
            // $a='as';
            $query->where(['name LIKE' => '%' . $s . '%']);
        $users = $this->paginate($query);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if($this->user_permission() == 3)
            $id = $this->Auth->user('id');

        $user = $this->Users->get($id, [
            'contain' => ['UsersPermissions']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {


        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                //saving associated permission table
                $UsersPermissions = TableRegistry::get('UsersPermissions');
                $user_permission = $UsersPermissions->newEntity();
                $user_permission->user_id = $user->id;
                $UsersPermissions->save($user_permission);
                $this->Flash->success(__('The user has been saved.'));

                if(!$this->Auth->user())         //redirecting new user to login page
                    return $this->redirect(['action' => 'login']);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //resttrictinr user permission
        if($this->user_permission() == 3)
            $id = $this->Auth->user('id');
        //restricting author permmission
        if($this->user_permission() == 2 && ($this->request->getParam('pass.0') != $this->Auth->User('id'))){
                $this->Flash->error(__('You are not Authorised for this action.'));
                return $this->redirect(['action' => 'index']);            
            }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //resttrictinr user permission
        if($this->user_permission() == 3)
            $id = $this->Auth->user('id');
        //restricting author permmission
        if($this->user_permission() == 2 && ($this->request->getParam('pass.0') != $this->Auth->User('id'))){
                $this->Flash->error(__('You are not Authorised for this action.'));
                return $this->redirect(['action' => 'index']);            
            }
        
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        
//        dd($user->id);
        if ($this->Users->delete($user) ) {
            
            if($user->id == $this->Auth->User('id')) $this->Auth->logout();
            $this->Flash->success(__('The user has been deleted.'));
            return $this->redirect(['action' => 'add']);
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function deactivate($id= null)
    {
        if($this->user_permission() == 3)
            return $this->redirect(['action'=> 'index']);

        $user = $this->Users->get($id);
        $user->status = 0;
        if($this->Users->save($user)){
            $this->Flash->success(__('The user has been Deactivated.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('The user could not be Deactivated. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function activate($id= null)
    {
        if($this->user_permission() == 3)
            return $this->redirect(['action'=> 'index']);
        
        $user = $this->Users->get($id);
        $user->status = 1;
        if($this->Users->save($user)){
            $this->Flash->success(__('The user has been Activated.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('The user could not be Activated. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
