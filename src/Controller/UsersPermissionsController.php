<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersPermissions Controller
 *
 * @property \App\Model\Table\UsersPermissionsTable $UsersPermissions
 *
 * @method \App\Model\Entity\UsersPermission[] paginate($object = null, array $settings = [])
 */
class UsersPermissionsController extends AppController
{
public function isAuthorized()
    {
        $user = $this->Auth->user();
        //stopping user
        if($user['_matchingData']['UsersPermissions']['permission']==3)
            return false;
        //stopping author
        if (in_array($this->request->getParam('action'), ['edit', 'delete']) && $user['_matchingData']['UsersPermissions']['permission'] > 1)
            return false;

        return parent::isAuthorized();
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $usersPermissions = $this->paginate($this->UsersPermissions);

        $this->set(compact('usersPermissions'));
        $this->set('_serialize', ['usersPermissions']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Permission id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersPermission = $this->UsersPermissions->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('usersPermission', $usersPermission);
        $this->set('_serialize', ['usersPermission']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        return $this->redirect(['action' => 'index']);
        // this method is not required

        // $usersPermission = $this->UsersPermissions->newEntity();
        // if ($this->request->is('post')) {
        //     $usersPermission = $this->UsersPermissions->patchEntity($usersPermission, $this->request->getData());
        //     if ($this->UsersPermissions->save($usersPermission)) {
        //         $this->Flash->success(__('The users permission has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     }
        //     $this->Flash->error(__('The users permission could not be saved. Please, try again.'));
        // }
        // $users = $this->UsersPermissions->Users->find('list', ['limit' => 200]);
        // $this->set(compact('usersPermission', 'users'));
        // $this->set('_serialize', ['usersPermission']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Permission id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersPermission = $this->UsersPermissions->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersPermission = $this->UsersPermissions->patchEntity($usersPermission, $this->request->getData());
            if ($this->UsersPermissions->save($usersPermission)) {
                $this->Flash->success(__('The users permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users permission could not be saved. Please, try again.'));
        }
//        $users = $this->UsersPermissions->Users->find('list', ['limit' => 200]);
        $this->set(compact('usersPermission'));
        $this->set('_serialize', ['usersPermission']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Permission id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        return $this->redirect(['action' => 'index']);
        // this method is not required

        // $this->request->allowMethod(['post', 'delete']);
        // $usersPermission = $this->UsersPermissions->get($id);
        // if ($this->UsersPermissions->delete($usersPermission)) {
        //     $this->Flash->success(__('The users permission has been deleted.'));
        // } else {
        //     $this->Flash->error(__('The users permission could not be deleted. Please, try again.'));
        // }

        // return $this->redirect(['action' => 'index']);
    }
}
