<?php
/**
  * @var \App\View\AppView $this
  */
?>

<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users Permissions'), ['controller' => 'UsersPermissions', 'action' => 'index']) ?></li>
        <!-- <li><?= $this->Html->link(__('New Users Permission'), ['controller' => 'UsersPermissions', 'action' => 'add']) ?></li> -->
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?> 
        <span style="float: right;"><?= $this->request->session()->read('Auth.User.name');?></span>
            <span style="float: right; margin-right: 5%;">
        <form method="get"  action='<?= $this->Url->build(["controller" => "Users", "action" => "index"]);?>'>
                <input type="text" name="s" list="s_users" placeholder="Search..." onkeyup="ajax_data(this.value, '<?= $this->Url->build(["controller" => "Users", "action" => "sortedList"]);?>', '#sorted_list')">
                  <datalist id="s_users">
                    <!-- <option value="Internet Explorer">
                    this datalist function didn't work. need more learning=====================
                    <option value="Firefox">
                    <option value="Chrome">
                    <option value="Opera">
                    <option value="Safari"> -->
                  </datalist>
                <input type="submit" style="display: none;">
        </form>
            </span>

    </h3>
    <div id="sorted_list">
        
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('dob') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('contact_no') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Number->format($user->id) ?></td>
                    <td><?= h($user->name) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user->dob) ?></td>
                    <td><?= h($user->contact_no) ?></td>
                    <td>

                    <?php if($user->status == 1):?>
                        <p><span style="background: green; color: white;">Active</span></p>
                        <?= $this->Html->link(__('Deactivate'), ['action' => 'deactivate', $user->id]) ?>
                    <?php elseif($user->status == 0):?>
                        <p><span style="background: red; color: white;">Inactive</span></p>
                        <?= $this->Html->link(__('Ativate'), ['action' => 'activate', $user->id]) ?>
                    <?php endif;?>

                    </td>
                    <td><?= h($user->created) ?></td>
                    <td><?= h($user->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                    <?php if($this->request->session()->read('Auth.User._matchingData.UsersPermissions.permission') == 1): ?>

                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                    <?php endif;?>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        </div>
    </div>
</div>


<script>
function ajax_data(user_data, link, des)
{
$.ajax({
url: link,
        type:"GET",
        data: {"user_data":user_data},
        success: function(response) {
                if (response) {
                    console.log(response);
                    // alert('hi');
                    $('#sorted_list').html(response);

                }
            },
});
};
function send_delete_id(id, des) {
$(des).val(id);
}

</script>
