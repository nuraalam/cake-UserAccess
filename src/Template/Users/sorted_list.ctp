  
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dob') ?></th>
            <th scope="col"><?= $this->Paginator->sort('contact_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('status') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $this->Number->format($user->id) ?></td>
            <td><?= h($user->name) ?></td>
            <td><?= h($user->email) ?></td>
            <td><?= h($user->dob) ?></td>
            <td><?= h($user->contact_no) ?></td>
            <td>

            <?php if($user->status == 1):?>
                <p><span style="background: green; color: white;">Active</span></p>
                <?= $this->Html->link(__('Deactivate'), ['action' => 'deactivate', $user->id]) ?>
            <?php elseif($user->status == 0):?>
                <p><span style="background: red; color: white;">Inactive</span></p>
                <?= $this->Html->link(__('Ativate'), ['action' => 'activate', $user->id]) ?>
            <?php endif;?>

            </td>
            <td><?= h($user->created) ?></td>
            <td><?= h($user->modified) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
            <?php if($this->request->session()->read('Auth.User._matchingData.UsersPermissions.permission') == 1): ?>

                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
            <?php endif;?>
            </td>
        </tr>

        <?php endforeach; ?>
    </tbody>
</table>
<!-- <div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
 -->