<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>

    <?php if($this->request->session()->read('Auth.User._matchingData.UsersPermissions.permission') != 3):?>

        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users Permissions'), ['controller' => 'UsersPermissions', 'action' => 'index']) ?> </li>
        <!-- <li><?= $this->Html->link(__('New Users Permission'), ['controller' => 'UsersPermissions', 'action' => 'add']) ?> </li> -->
    <?php endif;?>

    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dob') ?></th>
            <td><?= h($user->dob) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact No') ?></th>
            <td><?= h($user->contact_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td>
            <?= $user->status = 1 ? 'active' : 'inactive' ?>                
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <?php if($this->request->session()->read('Auth.User._matchingData.UsersPermissions.permission') != 3):?>
        <div class="related">
            <h4><?= __('Related Users Permission') ?></h4>
            <?php if (!empty($user->users_permission)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <!-- <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th> -->
                    <th scope="col"><?= __('Permission') ?></th>
                    <!-- <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th> -->
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>

                <tr>
                    <!-- <td><?= h($user->users_permission->id) ?></td>
                    <td><?= h($user->users_permission->user_id) ?></td> -->
                    <td>
                    <?php
                    if($user->users_permission->permission == 1)
                        echo "Admin";
                    if($user->users_permission->permission == 2)
                        echo "Editor";
                    if($user->users_permission->permission == 3)
                        echo "User";
                    ?>
                    </td>
                    <!-- <td><?= h($user->users_permission->created) ?></td>
                    <td><?= h($user->users_permission->modified) ?></td> -->
                    <td class="actions">
                        <!--<?= $this->Html->link(__('View'), ['controller' => 'UsersPermissions', 'action' => 'view', $user->users_permission->id]) ?>-->
                        <?= $this->Html->link(__('Edit'), ['controller' => 'UsersPermissions', 'action' => 'edit', $user->users_permission->id]) ?>
                        <!--<?= $this->Form->postLink(__('Delete'), ['controller' => 'UsersPermissions', 'action' => 'delete', $user->users_permission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->users_permission->id)]) ?>-->
                    </td>
                </tr>
                
            </table>
            <?php endif; ?>
        </div>
    <?php endif;?>
</div>
