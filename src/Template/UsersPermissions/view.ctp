<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Permission'), ['action' => 'edit', $usersPermission->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Permission'), ['action' => 'delete', $usersPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersPermission->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Permissions'), ['action' => 'index']) ?> </li>
        <!-- <li><?= $this->Html->link(__('New Users Permission'), ['action' => 'add']) ?> </li> -->
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersPermissions view large-9 medium-8 columns content">
    <h3><?= h($usersPermission->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $usersPermission->has('user') ? $this->Html->link($usersPermission->user->name, ['controller' => 'Users', 'action' => 'view', $usersPermission->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersPermission->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($usersPermission->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Permission') ?></th>
            <td><?= $this->Number->format($usersPermission->permission) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($usersPermission->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($usersPermission->modified) ?></td>
        </tr>
    </table>
</div>
