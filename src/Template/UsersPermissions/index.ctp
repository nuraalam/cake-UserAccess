<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <!-- <li><?= $this->Html->link(__('New Users Permission'), ['action' => 'add']) ?></li> -->
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersPermissions index large-9 medium-8 columns content">
    <h3><?= __('Users Permissions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('status') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('permission') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('created') ?></th> -->
                <!-- <th scope="col"><?= $this->Paginator->sort('modified') ?></th> -->
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersPermissions as $usersPermission): ?>
            <tr>
                <td><?= $this->Number->format($usersPermission->user->id) ?></td>
                <td><?= $usersPermission->has('user') ? $this->Html->link($usersPermission->user->name, ['controller' => 'Users', 'action' => 'view', $usersPermission->user->id]) : '' ?></td>
                <!-- <td><?= $this->Number->format($usersPermission->status) ?></td> -->
                <td><?php 
                    if($usersPermission->permission == 1)
                        echo "Admin";
                    if($usersPermission->permission == 2)
                        echo "Editor";
                    if($usersPermission->permission == 3)
                        echo "User";
                ?></td>
                <!-- <td><?= h($usersPermission->created) ?></td> -->
                <!-- <td><?= h($usersPermission->modified) ?></td> -->
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', 'controller' => 'Users', $usersPermission->user_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersPermission->id]) ?>
                    <!--<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersPermission->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersPermission->id)]) ?>-->
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
