-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2017 at 07:19 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_bccij_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20170509063412, 'CreateUsers', '2017-05-09 05:49:53', '2017-05-09 05:49:53', 0),
(20170509064834, 'CreateUsersPermissions', '2017-05-09 05:49:53', '2017-05-09 05:49:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active; 0=disabled',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `dob`, `contact_no`, `status`, `created`, `modified`) VALUES
(9, 'alam', 'alam@demo.go', '$2y$10$VrZyz3rc4RNLgHn7bxMi2OAA80P871lK5410lUvdpZ890Q7xLbQwS', '01/01/2000', '0000000000000', 1, '2017-05-09 13:29:38', '2017-05-11 05:03:44'),
(12, 'alam2', 'alam2@demo.go', '$2y$10$PC.obq6BonDGiHz8N6PD6ODqVgFyE3VUxcR9B9JDIzMAjFB9LQ7B.', '01/01/2000', '00000000000000', 1, '2017-05-10 12:42:06', '2017-05-11 05:03:59'),
(13, 'alam3', 'alam3@demo.go', '$2y$10$GtFiGcCDH5gVha1VgJNaYe3y/PPcMensGFoy04XL8vbskav8G0Umi', '01/01/2000', '000000000000000', 1, '2017-05-10 14:11:55', '2017-05-11 05:04:14'),
(14, 'alam4', 'alam4@demo.go', '$2y$10$5.d4PyP1Z4PLtbP.I7/1eudFeg8qLPA6xizahJucHGUEMMYn1DAg6', '01/01/2000', '0000000000000000', 1, '2017-05-10 19:11:52', '2017-05-11 05:05:07'),
(15, 'alam5', 'alam5@demo.go', '$2y$10$XTHsD4c.pLzTHqJjWgBOnuO3mhtXBeRrJNNzTuOFTopP20Vfepgy6', '01/01/2000', '00000000000000', 0, '2017-05-10 19:17:40', '2017-05-11 05:05:19'),
(16, 'alam6', 'alam6@demo.go', '$2y$10$LqlDnWXML2Jr5wo.Q9V/3.G8vZ3bG0kthO9hmiAR15pZlEmXj75Wu', '01/01/2000', '000000000000', 1, '2017-05-11 05:05:56', '2017-05-11 05:05:56'),
(17, 'alam7', 'alam7@demo.go', '$2y$10$3Z2qYb1YAEPJ6WWR5Lqhie51R7UqGZIjA2LcJsez0udmKWUx2svSG', '01/01/2000', '0000000000', 1, '2017-05-11 05:06:22', '2017-05-11 05:06:22'),
(18, 'alam8', 'alam8@demo.go', '$2y$10$D2.MfsGgsEwq5djKi0RgK.Bhx9ozrlIrrt8S78.NB8yWNvTPlpEzK', '01/01/2000', '000000000', 1, '2017-05-11 05:06:46', '2017-05-11 05:06:46'),
(19, 'alam9', 'alam9@demo.go', '$2y$10$YPmjf3J12qdBJJsH0Z2Eke5BUkjT7nwQAZlN9t5RxRrpiRqjoVx9.', '01/01/2000', '00000000000', 1, '2017-05-11 05:07:14', '2017-05-11 05:07:14'),
(20, 'alam10', 'alam10@demo.go', '$2y$10$wfVSwuZVQ.HMOFrLyScx6e/AiQ5F1Ur5kon19nIGf8D5f2aeJAPBq', '01/01/2000', '00000000000', 1, '2017-05-11 05:07:39', '2017-05-11 05:07:39'),
(21, 'alam11', 'alam11@demo.go', '$2y$10$vJb.wkQQ2p7BvcVf1MuP3.7qXNr6H2LhYdKKnE7jWEPvDlmZCg5rO', '01/01/2000', '0000000000', 1, '2017-05-11 05:08:10', '2017-05-11 05:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active; 0=disabled',
  `permission` tinyint(4) NOT NULL DEFAULT '3' COMMENT '1=SUPER ADMIN,2=ADMIN,3=NORMAL USER',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `user_id`, `status`, `permission`, `created`, `modified`) VALUES
(1, 9, 1, 1, '2017-05-09 21:14:03', '2017-05-09 21:14:03'),
(4, 12, 1, 2, '2017-05-10 12:42:19', '2017-05-10 14:15:57'),
(5, 13, 1, 3, '2017-05-10 14:11:55', '2017-05-10 14:11:55'),
(6, 14, 1, 1, '2017-05-10 19:11:52', '2017-05-10 19:12:34'),
(7, 15, 1, 1, '2017-05-10 19:17:40', '2017-05-11 05:08:51'),
(8, 16, 1, 2, '2017-05-11 05:05:56', '2017-05-11 05:09:06'),
(9, 17, 1, 2, '2017-05-11 05:06:23', '2017-05-11 05:09:18'),
(10, 18, 1, 2, '2017-05-11 05:06:47', '2017-05-11 05:09:28'),
(11, 19, 1, 3, '2017-05-11 05:07:14', '2017-05-11 05:07:14'),
(12, 20, 1, 3, '2017-05-11 05:07:39', '2017-05-11 05:07:39'),
(13, 21, 1, 3, '2017-05-11 05:08:10', '2017-05-11 05:08:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_EMAIL` (`email`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD CONSTRAINT `users_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
