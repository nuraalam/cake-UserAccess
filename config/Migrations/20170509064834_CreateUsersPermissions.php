<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateUsersPermissions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users_permissions');
        $table->addColumn('user_id', 'integer');
        $table->addColumn('status', 'integer', [
            'limit' => MysqlAdapter::INT_TINY,
            'default' => 1,
            'null' => false,
            'comment' => '1=active; 0=disabled',
        ]);
        $table->addColumn('permission', 'integer', [
            'default' => 3,
            'limit' => MysqlAdapter::INT_TINY,
            'null' => false,
            'comment' => '1=SUPER ADMIN,2=ADMIN,3=NORMAL USER',
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('user_id', 'users', 'id');
        $table->create();
    }
}
